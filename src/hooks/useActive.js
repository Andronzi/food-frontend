import { useState } from "react";

export default function useActive() {
  const [value, setValue] = useState();

  function toggleValue() {
    setValue((prevValue) => !prevValue);
  }

  return {
    value,
    toggleValue,
  };
}
