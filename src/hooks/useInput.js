import { useState } from "react";

export default function useInput() {
  const [value, setValue] = useState("");

  function onChange(e) {
    setValue(e.target.value);
  }

  return {
    value,
    onChange,
  };
}
