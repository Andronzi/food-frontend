import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Shopping from "./components/Shopping";
import History from "./components/History/History";
import Statistics from "./components/Statistics/Statistics";

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<Shopping />} />
          <Route path="/history" element={<History />} />
          <Route path="/statistics" element={<Statistics />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
