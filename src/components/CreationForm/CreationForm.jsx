import TextInput from "../Input/TextInput/TextInput";
import AreaInput from "../Input/AreaInput/AreaInput";
import SelectInput from "../Input/SelectInput/SelectInput";
import "./form.scss";

const CreationForm = () => {
  return (
    <form className="form">
      <h2 className="form-title">Add a new item</h2>
      <TextInput label="Name" placeholder="Enter a name" />
      <AreaInput label="Note (optional)" placeholder="Enter a note" />
      <TextInput label="Image (optional)" placeholder="Enter a url" />
      <SelectInput
        label="Category"
        placeholder="Enter a category"
        options={["Fruit and vegetables", "Meat and Fish", "Beverages"]}
      />
      <div className="buttons">
        <button className="buttons__cancel-button">cancel</button>
        <button className="buttons__save-button">Save</button>
      </div>
    </form>
  );
};

export default CreationForm;
