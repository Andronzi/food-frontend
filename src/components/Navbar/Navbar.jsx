import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="navbar">
      <img src="resources/logo.svg" alt="logo" className="navbar-image"></img>
      <ul className="navbar-list">
        <li className="navbar-list__item">
          <Link to="/">
            <img
              src="resources/list.png"
              className="navbar-list__image"
              alt="menu"
            ></img>
          </Link>
        </li>
        <li className="list__item">
          <Link to="history">
            <img
              src="resources/return.png"
              className="navbar-return__image"
              alt="history"
            ></img>
          </Link>
        </li>
        <li className="list__item">
          <Link to="statistics">
            <img
              src="resources/statistics.png"
              className="navbar-statistics__image"
              alt="statistics"
            ></img>
          </Link>
        </li>
      </ul>
      <button className="navbar-button">
        <img className="navbar-button__image" src="resources/cart.png"></img>
      </button>
    </div>
  );
};

export default Navbar;
