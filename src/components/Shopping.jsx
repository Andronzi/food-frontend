import Navbar from "./Navbar/Navbar";
import ShoppingList from "./ShoppingList/ShoppingList";
import ShoppingForm from "./ShoppingForm/ShoppingForm";
import "../scss/shopping.scss";

const Shopping = () => {
  return (
    <div className="shopping">
      <Navbar />
      <ShoppingList />
      <ShoppingForm />
    </div>
  );
};

export default Shopping;
