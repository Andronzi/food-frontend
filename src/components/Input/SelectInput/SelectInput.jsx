import useActive from "../../../hooks/useActive";
import "../input.scss";

const SelectInput = ({ label, placeholder, options }) => {
  const active = useActive();
  const currentValue = placeholder;

  return (
    <>
      <div className="input">
        <div>
          <label>{label}</label>
        </div>
        <input
          className="select-input"
          onClick={active.toggleValue}
          value={currentValue}
          type="button"
        />
      </div>
      {active.value ? (
        <div className="options-list">
          {options.map((option) => (
            <p>{option}</p>
          ))}
        </div>
      ) : (
        ""
      )}
    </>
  );
};

export default SelectInput;
