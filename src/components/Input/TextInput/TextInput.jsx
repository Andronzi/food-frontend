import useInput from "../../../hooks/useInput";
import "../input.scss";

const TextInput = ({ label, placeholder }) => {
  const name = useInput();
  return (
    <div className="input">
      <div>
        <label>{label}</label>
      </div>
      <input placeholder={placeholder} {...name} />
    </div>
  );
};

export default TextInput;
