import useInput from "../../../hooks/useInput";

const AreaInput = ({ label, placeholder }) => {
  const name = useInput();
  return (
    <div className="input">
      <div>
        <label>{label}</label>
      </div>
      <textarea placeholder={placeholder} {...name} />
    </div>
  );
};

export default AreaInput;
