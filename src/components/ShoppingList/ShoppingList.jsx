const ShoppingList = () => {
  return (
    <div className="shopping-content">
      <div className="container">
        <h2 className="shopping-title">
          <span>Shopingify</span> allow your take your <br /> shopping list
          wherever you go
        </h2>
        <div className="shopping-input">
          <input placeholder="search item" className="search" />
          <img className="search-image" src="resources/search.png" alt="" />
        </div>
      </div>
      <div className="shopping-list"></div>
    </div>
  );
};

export default ShoppingList;
